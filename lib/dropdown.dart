import 'package:flutter/material.dart';

class DropdownWidget extends StatefulWidget {
  DropdownWidget({Key? key}) : super(key: key);

  @override
  _DropdownWidgetState createState() => _DropdownWidgetState();
}

class _DropdownWidgetState extends State<DropdownWidget> {
  String vaccine = '-';
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('DropDown'),
      ),
      body: Column(
        children: [
          Container(
            child: Row(
              children: [
                Text('Vaccine: '),
                Expanded(
                  child: Container(),
                ),
                DropdownButton(
                  items: [
                    DropdownMenuItem(child: Text('-'), value: '-'),
                    DropdownMenuItem(child: Text('Pfizer'), value: 'Pfizer'),
                    DropdownMenuItem(
                        child: Text('Johnson & Johnson'),
                        value: 'Johnson % Johnson'),
                    DropdownMenuItem(
                        child: Text('Sputnik V'), value: 'Sputnik V'),
                    DropdownMenuItem(
                        child: Text('AztraZenaca'), value: 'AztraZenaca'),
                    DropdownMenuItem(child: Text('Novavax'), value: 'Novavax'),
                    DropdownMenuItem(
                        child: Text('DogShit Vaccine 1 Sinopharm'),
                        value: 'DogShit Vaccine 1 Sinopharm'),
                    DropdownMenuItem(
                        child: Text('DogShit Vaccine 2 Sinovac'),
                        value: 'DogShit Vaccine 1 Sinovac'),
                  ],
                  value: vaccine,
                  onChanged: (String? newValue) {
                    setState(() {
                      vaccine = newValue!;
                    });
                  },
                )
              ],
            ),
          ),
          Center(
            child: Text(
              vaccine,
              style: TextStyle(fontSize: 20.0, fontWeight: FontWeight.bold),
            ),
          )
        ],
      ),
    );
  }
}
