import 'package:checkbox/checkbox.dart';
import 'package:flutter/material.dart';

import 'checkboxtile.dart';
import 'dropdown.dart';
import 'radio.dart';

void main() {
  runApp(MaterialApp(title: 'UI Extension', home: MyApp()));
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('UI Extension')),
      drawer: Drawer(
        child: ListView(
          children: [
            const DrawerHeader(
              child: Text('UI Manu'),
              decoration: BoxDecoration(color: Colors.amber),
            ),
            ListTile(
              title: Text('Check Box'),
              onTap: () {
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => CheckBoxWidget()));
              },
            ),
            ListTile(
              title: Text('Check Box Tile'),
              onTap: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => CheckBoxTileWidget()));
              },
            ),
            ListTile(
              title: Text('Dropdown'),
              onTap: () {
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => DropdownWidget()));
              },
            ),
            ListTile(
              title: Text('Radio'),
              onTap: () {
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => RadioWidget()));
              },
            ),
          ],
        ),
      ),
      body: ListView(
        children: [
          ListTile(
            title: Text('Check Box'),
            onTap: () {
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => CheckBoxWidget()));
            },
          ),
          ListTile(
            title: Text('Check Box Tile'),
            onTap: () {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => CheckBoxTileWidget()));
            },
          ),
          ListTile(
            title: Text('Dropdown'),
            onTap: () {
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => DropdownWidget()));
            },
          ),
          ListTile(
            title: Text('Radio'),
            onTap: () {
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => RadioWidget()));
            },
          ),
        ],
      ),
    );
  }
}
